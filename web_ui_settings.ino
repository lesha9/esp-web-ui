#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <PubSubClient.h>
#include <Arduino.h>
#include <Preferences.h>

Preferences prefs;

WiFiClient wifi_client;
PubSubClient MqttClient(wifi_client);

String getMacAddress()
{
  uint8_t baseMac[6];
  esp_read_mac(baseMac, ESP_MAC_WIFI_STA);
  char baseMacChr[18] = {0};
  sprintf(baseMacChr, "%02X%02X%02X%02X%02X%02X", baseMac[0], baseMac[1], baseMac[2], baseMac[3], baseMac[4], baseMac[5]);
  return String(baseMacChr);
}

String macc = getMacAddress();
char bufTopic[140], bufWillTopic[140];
String topic = macc + "/data";
String willTopic = "aastudio/sens/" + macc + "/status";
boolean client_mode;
String wifi_ssid;
String wifi_pass;
String mqtt_addr;
int mqtt_port;
String mqtt_login;
String mqtt_pass;
String upd_url;
String net_ip;
String net_gateway;
String net_mask;
String net_dns;

void callback(char* topic, byte* payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}


void reconnectMqtt()
{
  //крутимся смотрим подключение если оно отпало
  while (!MqttClient.connected())
  {
    const char *clientId = macc.c_str();

    //сет сервер надо делать тут тоже как и в setup по новой как показал опыт
    MqttClient.setServer(mqtt_addr.c_str(), mqtt_port);

    //Тут мы подключаемся к брокеру с определенным clientId (мак) с логином паролем, и далее мы устанавлиаем топик в который с доставкой 2(qos) брокер отправит (сам в себя) сообщение "Offline" с флагом True (Retain) в случае если наше устройство отключится от сети внезапно (например когда пропало питание у датчика), а не запланировано
    if (MqttClient.connect(clientId, mqtt_login.c_str(), mqtt_pass.c_str(), bufWillTopic, 2, true, "Offline"))
    {
      Serial.println("Mqttconnect - OK");
    }
    else
    {
      Serial.print("Mqtt.state() = ");
      Serial.println(MqttClient.state());
      //httpSerial("MqttState:" + String(MqttClient.state()));
      /*              
            -4 : MQTT_CONNECTION_TIMEOUT - the server didn't respond within the keepalive time
            -3 : MQTT_CONNECTION_LOST - the network connection was broken
            -2 : MQTT_CONNECT_FAILED - the network connection failed
            -1 : MQTT_DISCONNECTED - the client is disconnected cleanly
            0 : MQTT_CONNECTED - the client is connected
            1 : MQTT_CONNECT_BAD_PROTOCOL - the server doesn't support the requested version of MQTT
            2 : MQTT_CONNECT_BAD_CLIENT_ID - the server rejected the client identifier
            3 : MQTT_CONNECT_UNAVAILABLE - the server was unable to accept the connection
            4 : MQTT_CONNECT_BAD_CREDENTIALS - the username/password were rejected
            5 : MQTT_CONNECT_UNAUTHORIZED - the client was not authorized to connect
      */
      delay(500);
    }
  }
}



const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML>
<html>
  <head>
    <title>ESP Input Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      body {
        background-color: black;
        color: white;
        margin-left: auto;
        margin-right: auto;
        width: 30em
      }
      .button {
          display: inline-block;
          padding: 5px 25px;
          font-size: 24px;
          cursor: pointer;
          text-align: center;
          text-decoration: none;
          outline: none;
          color: #fff;
          background-color: #FFA500;
          border: none;
          border-radius: 15px;
          box-shadow: 0 9px #999;
      }

      .button:hover {background-color: #FF8C00}

      .button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
      }
    </style>
  </head>
  <body>
    <form action="/get">
    <table border = 0px>
    <tr>
      <td>MAC Address:<br/></td>
      <td vertical-align = middle>%MACADDR%<br/></td>
    </tr>
    <tr>
      <td>SSID:<br/></td>
      <td vertical-align = middle><input type="text" name="wifi_ssid"><br/></td>
    </tr>
    <tr>
      <td>Wifi Password:<br/></td>
      <td vertical-align = middle><input type="text" name="wifi_pass"><br/></td>
    </tr>
    <tr>
      <td>MQTT Addres:<br/></td>
      <td vertical-align = middle><input type="text" name="mqtt_addr"><br/></td>
    </tr>
    <tr>
      <td>MQTT Port: <br/></td>
      <td vertical-align = middle><input type="text" name="mqtt_port"><br/></td>
    </tr>
    <tr>
      <td>MQTT Login: <br/></td>
      <td vertical-align = middle><input type="text" name="mqtt_login"><br/></td>
    </tr>
    <tr>
      <td>MQTT Password: <br/></td>
      <td vertical-align = middle><input type="text" name="mqtt_pass"><br/></td>
    </tr>
    <tr>
      <td>Update server URL: <br/></td>
      <td vertical-align = middle><input type="text" name="update_url"><br/></td>
    </tr>
    <tr>
      <td>IP: <br/></td>
      <td vertical-align = middle><input type="text" name="net_ip"><br/></td>
    </tr>
    <tr>
      <td>Network gateway: <br/></td>
      <td vertical-align = middle><input type="text" name="net_gateway"><br/></td>
    </tr>
    <tr>
      <td>Network mask: <br/></td>
      <td vertical-align = middle><input type="text" name="net_mask"><br/></td>
    </tr>
    <tr>
      <td>DNS: <br/></td>
      <td vertical-align = middle><input type="text" name="net_dns"><br/></td>
    </tr>
      
    </table>
    <br/>
    <input type="submit" class="button" value="Submit">
    </form>
  </body>
</html>
)rawliteral";



/* SSID и пароль в режиме сервера */
const char* ssid = "ESP32";
const char* password = "01234567";

/* Настройки IP адреса сервера*/
IPAddress local_ip(192,168,2,1);
IPAddress gateway(192,168,2,1);
IPAddress subnet(255,255,255,0);

AsyncWebServer server(80);

void notFound(AsyncWebServerRequest *request) 
{
  request->send(404, "text/plain", "Not found");
}



String processor(const String& var)
{
  if(var == "MACADDR")
    return macc;
  return String();
}

void setup() {
  Serial.begin(115200);
  prefs.begin("esp-memory", false);
  
  topic.toCharArray(bufTopic, topic.length() + 1);
  willTopic.toCharArray(bufWillTopic, willTopic.length() + 1);

  client_mode = prefs.getBool("client_mode", false);
  wifi_ssid = prefs.getString("wifi_ssid", "");
  wifi_pass = prefs.getString("wifi_pass", "");
  mqtt_addr = prefs.getString("mqtt_addr", "");
  mqtt_port = prefs.getInt("mqtt_port", 0);
  mqtt_login = prefs.getString("mqtt_login", "");
  mqtt_pass = prefs.getString("mqtt_pass", "");
  Serial.println(client_mode);
  prefs.clear();
  if(!client_mode)
  {
    Serial.println("S");
    WiFi.softAP(ssid, password);
    WiFi.softAPConfig(local_ip, gateway, subnet);
    delay(100);
  
    
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
    {
      request->send_P(200, "text/html", index_html, processor);
    });
  
  
  
    server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) 
    {      
      if (request->hasArg("wifi_ssid")) 
      {
        wifi_ssid = request->arg("wifi_ssid");
      }
      
      if (request->hasArg("wifi_pass")) 
      {
        wifi_pass = request->arg("wifi_pass");
      }
      
      if (request->hasArg("mqtt_addr")) 
      {
        mqtt_addr = request->arg("mqtt_addr");
      }

      if (request->hasArg("mqtt_port")) 
      {
        mqtt_port = request->arg("mqtt_port").toInt();
      }

      if (request->hasArg("mqtt_login")) 
      {
        mqtt_login = request->arg("mqtt_login");
      }

      if (request->hasArg("mqtt_pass")) 
      {
        mqtt_pass = request->arg("mqtt_pass");
        prefs.putBool("client_mode", true);
        prefs.putString("wifi_ssid", wifi_ssid);
        prefs.putString("wifi_pass", wifi_pass);
        prefs.putString("mqtt_addr", mqtt_addr);
        prefs.putInt("mqtt_port", mqtt_port);
        prefs.putString("mqtt_login", mqtt_login);
        prefs.putString("mqtt_pass", mqtt_pass);
      }
      
      
      int args = request->args();
      for(int i=0;i<args;i++)
      {
        Serial.printf("ARG[%s]: %s\n", request->argName(i).c_str(), request->arg(i).c_str());
        
      }
      
      request->send(200, "text/html", "HTTP GET request sent to your ESP on input field () with value: ");
      //Serial.println(prefs.getBool("client_mode", false)); //ЛОМАЕТ ВСЁ
      
    });
    
    server.onNotFound(notFound);
    
    server.begin();
    Serial.println("HTTP server started"); 
  } //if(!client_mode)
  else
  {
    Serial.println("Change mode complete");

    char char_wifi_ssid[wifi_ssid.length() + 1];
    char char_wifi_pass[wifi_pass.length() + 1];
    char char_mqtt_addr[mqtt_addr.length() + 1];
    char char_mqtt_login[mqtt_login.length() + 1];
    char char_mqtt_pass[mqtt_pass.length() + 1];

    wifi_ssid.toCharArray(char_wifi_ssid, wifi_ssid.length() + 1);
    wifi_pass.toCharArray(char_wifi_pass, wifi_pass.length() + 1);
    mqtt_addr.toCharArray(char_mqtt_addr, mqtt_addr.length() + 1);
    mqtt_login.toCharArray(char_mqtt_login, mqtt_login.length() + 1);
    mqtt_pass.toCharArray(char_mqtt_pass, mqtt_pass.length() + 1);
    
    Serial.println("Try Connecting to ");
    Serial.println(char_wifi_ssid);
    Serial.println("With password");
    Serial.println(char_wifi_pass);
    WiFi.begin(char_wifi_ssid, char_wifi_pass);
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(1000);
      Serial.print(".");
    }
    
    prefs.putBool("client_mode", true);
    prefs.putString("wifi_ssid", wifi_ssid);
    prefs.putString("wifi_pass", wifi_pass);
    prefs.putString("mqtt_addr", mqtt_addr);
    prefs.putInt("mqtt_port", mqtt_port);
    prefs.putString("mqtt_login", mqtt_login);
    prefs.putString("mqtt_pass", mqtt_pass);
    
    Serial.println("");
    Serial.println("WiFi connected successfully");
    Serial.print("Got IP: ");
    Serial.println(WiFi.localIP());
    MqttClient.setServer(char_mqtt_addr, 1338);
    MqttClient.publish(bufTopic, "Hello world !", true);
    delay(1500);
  }
}

void loop()
{
//  if (M5.BtnA.isPressed()) 
//  {
//    prefs.putBool("client_mode", false);
//    Serial.println("Client mode disable.");
//  }
}
